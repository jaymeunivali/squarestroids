import java.awt.Color;

public class GameShot extends GameObject {

    public float degree;

	public GameShot(float x, float y, int width, int height, int velx, int vely, Color color, float degree) {
        super(x, y, width, height, velx, vely, color);
        this.degree = degree;
		this.collide = new GameCollide(this, true, true);
        System.out.println(degree);
    }
    
	public void move(long DiffTime){
		x += velx * DiffTime/1000.0f;
		y += vely * DiffTime/1000.0f;
	}
	
    public void onCollision(String side){
		if(side == "left" || side == "right") {
			velx = velx * -1;
		}
		if(side == "top" || side == "bot") {
			vely = vely * -1;
		}
    }

	
	public void update(long DiffTime) {
		
		super.update(DiffTime);
		
	}
}