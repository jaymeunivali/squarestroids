import java.awt.Color;
import java.awt.event.MouseEvent;

public class GamePlayer extends GameObject {
		public boolean UP, DOWN, LEFT, RIGHT, FIRE;
		public GameShot shot;
		
		public GamePlayer(float x, float y, int width, int height, int velx, int vely, Color color) {
			super(x, y, width, height, velx, vely, color);
			this.collide = new GameCollide(this, true, true);
		}

		public void fire(int mouseX, int mouseY){
			float degree = getAngle(mouseX, mouseY);
			shot = new GameShot(x, y, 10, 10, 200, 200, Color.RED, degree);
		}

		
	
		public float getAngle(int mouseX, int mouseY) {
			float angle = (float) Math.toDegrees(Math.atan2(mouseX - y, mouseY - x));
		
			if(angle < 0){
				angle += 360;
			}
		
			return angle;
		}
    

		public void move(long DiffTime){
			if(UP) {
				y -= vely * DiffTime/1000.0f;
			} 
			if(DOWN) {
				y += vely * DiffTime/1000.0f;
			} 
			if(RIGHT) {
				x += velx * DiffTime/1000.0f;
			} 
			if(LEFT) {
				x -= velx * DiffTime/1000.0f;
			}
		}
		
		public void update(long DiffTime) {
			
			
			super.update(DiffTime);


			
		}
		
	}
