import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;

public abstract class GameObject {
    public float x;
    public float y;
    public float oldX;
    public float oldY;
    public int width;
    public int height;
    public int velx;
    public int vely;
    public Color color;
    public GameCollide collide = null;
    public static ArrayList<GameObject> list = new ArrayList<GameObject>();
    
    public GameObject(float x, float y, int width, int height, int velx, int vely, Color color) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.velx = velx;
        this.vely = vely;
        this.color = color;
        list.add(this);
    }
    
    public void onCollision(String side){
		if(side == "left" || side == "right") {
            x = oldX;
		}
		if(side == "top" || side == "bot") {
            y = oldY;
		}
    }
	
    public abstract void move(long DiffTime);
		
    public void update(long DiffTime) {
        
		oldX = x;
        oldY = y;
        this.move(DiffTime);
        if (collide != null && (oldX != x || oldY != y)) {
            this.collide.check();
        }
    }

    public void render(Graphics2D dbg) {
        dbg.setColor(color);
        dbg.fillRect((int)x, (int)y, width, height);
    }
}