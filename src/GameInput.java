import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class GameInput {
    public GameInput(){
		Game.gui.addKeyListener(new KeyAdapter() {
			// listen for esc, q, end, ctrl-c
			public void keyPressed(KeyEvent e) {
				checkPress(e);
			}

			public void keyReleased(KeyEvent e) {
				checkRelease(e);
			}
        });
        
		Game.gui.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				Game.player.fire(e.getX(), e.getY());
			}
		});
	}

	public void checkPress(KeyEvent e) {
		int keyCode = e.getKeyCode();

		if ((keyCode == KeyEvent.VK_ESCAPE) || (keyCode == KeyEvent.VK_Q) || (keyCode == KeyEvent.VK_END)
				|| ((keyCode == KeyEvent.VK_C) && e.isControlDown())) {
			Game.running = false;
		}

		if (keyCode == KeyEvent.VK_LEFT) {
			Game.player.LEFT = true;
		}
		if (keyCode == KeyEvent.VK_RIGHT) {
			Game.player.RIGHT = true;
		}
		if (keyCode == KeyEvent.VK_UP) {
			Game.player.UP = true;
		}
		if (keyCode == KeyEvent.VK_DOWN) {
			Game.player.DOWN = true;
		}

		if (keyCode == KeyEvent.VK_SPACE) {
			Game.player.FIRE = true;
		}

	}

	public void checkRelease(KeyEvent e) {
		int keyCode = e.getKeyCode();

		if (keyCode == KeyEvent.VK_LEFT) {
			Game.player.LEFT = false;
		}
		if (keyCode == KeyEvent.VK_RIGHT) {
			Game.player.RIGHT = false;
		}
		if (keyCode == KeyEvent.VK_UP) {
			Game.player.UP = false;
		}
		if (keyCode == KeyEvent.VK_DOWN) {
			Game.player.DOWN = false;
		}
		if (keyCode == KeyEvent.VK_SPACE) {
			Game.player.FIRE = false;
		}
	}
}