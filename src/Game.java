import java.util.ArrayList;
import java.awt.Color;
import java.awt.RenderingHints;
import java.awt.Canvas;
import java.awt.image.BufferStrategy;
import java.awt.Graphics2D;

public class Game {
	
	static final int PWIDTH = 640; // size of panel
	static final int PHEIGHT = 600;
    
	public static BufferStrategy strategy = null;
	public static Graphics2D dbg;
	public static int FPS, SFPS; 

	public static boolean running;
	public static Canvas gui = null;
	
	public static GameUpdate update;
	public static GameRender renderer;
	public static GameInput input;

	public static GamePlayer player;
	public static ArrayList<GameEnemy> quadrados;

    public Game(){
		update = new GameUpdate();
		renderer = new GameRender(PWIDTH, PHEIGHT);
		
		player = new GamePlayer(10, 50, 20, 60, 100, 100, Color.GREEN);
		quadrados = new ArrayList<GameEnemy>();
		quadrados.add(new GameEnemy(100, 200, 150, 50, 220, 220, 
		Color.YELLOW));
		quadrados.add(new GameEnemy(100, 10, 150, 50, 220, 220, 
		Color.YELLOW));

		gui = new Canvas();
		gui.setSize(PWIDTH, PHEIGHT);
		input = new GameInput();
		
        
	}
    
	public static void run() {
		running = true; 
		long DiffTime,TempoAnterior;
		
		int segundo = 0;
		DiffTime = 0;
		TempoAnterior = System.currentTimeMillis();

		gui.createBufferStrategy(2);
		strategy = gui.getBufferStrategy();

		while (running) {

			dbg = (Graphics2D) strategy.getDrawGraphics();
			dbg.setClip(0, 0, PWIDTH, PHEIGHT);
			dbg.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			update.update(DiffTime);

			renderer.render(dbg, FPS);

			dbg.dispose();
			strategy.show();

			try {
				Thread.sleep(1); // sleep a bit
			} catch (InterruptedException ex) {
			}

			DiffTime = System.currentTimeMillis() - TempoAnterior;
			TempoAnterior = System.currentTimeMillis();
				
			
			if(segundo!=((int)(TempoAnterior/1000))){
				FPS = SFPS;
				SFPS = 1;
				segundo = ((int)(TempoAnterior/1000));
			}else{
				SFPS++;
			}

		}
		System.exit(0); // so enclosing JFrame/JApplet exits
	}
}